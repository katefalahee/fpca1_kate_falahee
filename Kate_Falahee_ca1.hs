--Student name: Kate Falahee
--Functional Programming CA 1
--All functions 1 - 18 completed.

--function 1: Adds 2 numbers together and then doubles the result.
--Used notes as a reference
-- 18/02/2015
add_and_double :: (Num a) => a -> a -> a
add_and_double num number = (num + number) * 2
---------------------------------------------------------------------------------------

--function 2: an infix operator adds 2 numbers together and then doubles the result. Defined in terms of add_and_double.
--Was showed how the infix operator should work by Ronan then wrote the function myself
-- 18/02/2015
(+*) :: (Num a) => a -> a -> a
(+*) num number = num `add_and_double`number
---------------------------------------------------------------------------------------

--function 3: Takes in 3 arguments. Outputs a tuple containing the two roots.
--Used the -b formula and the equation given in the question
-- 19/02/2015
solve_quadratic_equation :: (Floating a) => (a,a,a) -> (a,a)
solve_quadratic_equation (a, b, c) = (-b + ans,-b - ans)
	where
		ans = (sqrt(b ** 2 - 4 * a * c))
---------------------------------------------------------------------------------------

--function 4: Takes an Int and returns a list of the first n Ints starting from 1.
--Used examples as a reference
-- 23/02/2015
first_n :: Int -> [Int]
first_n x = take x [1..]
---------------------------------------------------------------------------------------

--function 5: Takes an Integer argument and return a list of Integers.
--Tried to use examples as a reference but needed extra help from Ronan. I thought that the funtion had to take in an Integer and a list. 
--Ronan explained that the helper function would do this itself. 
-- 23/02/2015
first_n_integers :: Integer -> [Integer]
first_n_integers n = take_integer n [1..]
    where
        take_integer n xs
            |n <= 0 = []
        take_integer _[] = []
        take_integer n(x:xs) = x:take_integer(n-1)xs	
---------------------------------------------------------------------------------------

--function 6: Takes an Integer n and computes the product of all the factorials from 0 up to and including n.
--At first a miss read the question and only wrote a simple factorial function. Later I realised it was a product of all
--factorials and used recursion notes as a reference.
-- 21/02/2015
double_factorial :: Integer -> Integer
double_factorial 0 = 1
double_factorial n = (double_factorial (n-1)) * (factorial n)
    where 
        factorial n
            |n == 0 = 1
            |otherwise = n * factorial (n - 1)
---------------------------------------------------------------------------------------

--function 7: An infinite list of factorials called factorials using the zipWith function.
--Used notes for help
--03/03/2015
factorials :: [Integer]
factorials = 1 : zipWith (*) factorials [1..]
----------------------------------------------------------------------------------------

--function 8: Determines whether a given integer is a prime number.
--Used notes and definition of a prime number.
--06/03/2015
isPrime :: Integer -> Bool
isPrime num = findP num (num-1)
    where
        findP num1 num2
            |num2 <= 1 = True
            |num1 `mod` num2 == 0 = False
            |otherwise = findP num (num2-1)
---------------------------------------------------------------------------------------

--function 9: Returns a list of all primes.
--Only generate as much of the list as we need.
--Used notes and definition of a prime number, isPrime function 
--and Ronan explained that I would need to look at filters.
--06/03/2015
primes :: [Integer]
primes =(filter (isPrime) [1..])
-----------------------------------------------------------------------------------------

--function 10: A function to sum a list of integers recursively.
--Used notes and examples as a reference
-- 20/02/2015
sum_recursive :: [Integer] -> Integer
sum_recursive [] = 0
sum_recursive [x] = x
sum_recursive (x:xs) = x + sum_recursive xs
---------------------------------------------------------------------------------------

--function 11: A function to sum a list of integers in terms of foldl
--Found an example in the notes and answered based on that.
-- 23/02/2015
sum_foldl :: [Integer] -> Integer
sum_foldl xs = foldl(\acc x -> acc+x)0 xs
---------------------------------------------------------------------------------------

--function 12: A function to multiply a list of integers in terms of foldr.
--Found an example in the notes and answered based on that.
-- 23/02/2015
multiply_foldr :: [Integer] -> Integer
multiply_foldr xs = foldr(\x acc -> acc*x)1 xs
---------------------------------------------------------------------------------------

--function 13: Matching a string and integer. Return a string.
--Used notes as reference
--26/03/2015
guess :: String -> Integer -> String
guess str num
    |str == "I love functional programming" && num<=5 = "You have won!"
	|str /= "I love functional programming" && num<=5 = "Guess again!"
	|otherwise = "You have lost"
---------------------------------------------------------------------------------------

--function 14: Function to find the dot product of any two vectors.
--Not sure if it is done correctly. Not sure if I should have empty lists = 1.
--05/03/2015
product' :: [Integer] -> [Integer] -> Integer
product' _[] = 1
product' []_ = 1
product' (x:xs) (y:ys) = (x*y) + product' xs ys
----------------------------------------------------------------------------------------

--function 15: Is integer entered even.
--Used notes and knew formula to find an even number
--26/03/2015
isEven :: Integer -> Bool
isEven n
    |n `mod` 2 ==0 = True
	|otherwise = False
---------------------------------------------------------------------------------------

--function 16: Removes all vowels from a string.
--First tried with list comprehension, but after doing function 18 decided to use map.
--05/03/2015
unixname :: String -> String
unixname str = map replace str
	where
		replace letter
			|letter `elem` ['A','a','E','e','I','i','O','o','U','u'] = ' '
			|otherwise = letter
----------------------------------------------------------------------------------------

--function 17: A function intersection, which given two lists calculates what they have in common.
--Used notes on let and in. Also used hoogle for elem. Took a while to figure out an algorithm.
--06/03/2015
intersection' :: [Integer] -> [Integer] -> [Integer]
intersection' list1 list2 = 
    let nl = [ x | x <- list1, elem x list2] 
	in [ y | y <- list2, elem y nl]
-----------------------------------------------------------------------------------------
 
--function 18: Replace all vowels in a string with the letter x.
--Used notes as reference.
--05/03/2015
censor :: String -> String
censor str = map replace str
	where
		replace letter
			|letter `elem` ['A','a','E','e','I','i','O','o','U','u'] = 'x'
			|otherwise = letter
----------------------------------------------------------------------------------------